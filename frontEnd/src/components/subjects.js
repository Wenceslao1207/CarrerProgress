import React from 'react';
import { List, Popover } from 'antd';


const Subjects = (props) => {
	return (
	  <div>
			<List
				itemLayout="horizontal"
				dataSource={props.data}
				renderItem={
					item => (
						<Popover content="Click para informacion de la materia">
							<List.Item>
								<List.Item.Meta
									title={
										<h2>
											<a href={`materia/${item.id}`} style={{color: "#000000" }}>
												{item.name}
											</a>
										</h2>
									}
								/>
							</List.Item>
						</Popover>
					)
				}
			/>
	  </div>
	)
};

export default Subjects;



