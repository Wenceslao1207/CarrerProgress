import React from 'react';
import { List, Avatar, Icon } from 'antd';
import Subjects from '../components/subjects.js';
import axios from 'axios';
import equal from 'fast-deep-equal';

const filter_year = (year, array) => {
	return array.filter( (el) => {
		return el.year === parseInt(year);
	})
}


const Year = (year) => {
	switch (year) {
		case 1:
			return 'Primer Anio';
			break;
		case 2:
			return 'Segundo Anio';
			break;
		case 3:
			return 'Tercer Anio';
			break;
		case 4:
			return 'Cuarto Anio';
			break;
		case 5:
			return 'Quinto Anio';
			break;
		defult:
			return 'Error';
	}
}

class carrerYear extends React.Component {


	constructor(props) {
		super(props);
		this.state = {
			subjects: [],
		}
	}

	componentDidUpdate(prevProps){
		if(!equal(this.props.match.params.Anio, prevProps.match.params.Anio))
		{
			this.setData();
		}
	}
	componentDidMount() {
		this.setData();
	}

	setData() {
		const year = this.props.match.params.Anio
		axios.get('http://127.0.0.1:8000/carrer/listSubjects/', { 'headers': {Authorization: 'token d518f3d6163fd92f074414055330022bd26fcc5a'}})
			.then( res => {
				this.setState({
					subjects: filter_year(year, res.data),
					year: Year(parseInt(year)),
				});
			}
		);
	}

	render () {
		return (
			<div>
				<h1> {this.state.year} </h1>
				<Subjects data={this.state.subjects}/>
			</div>
		)
	}
};

export default carrerYear;
