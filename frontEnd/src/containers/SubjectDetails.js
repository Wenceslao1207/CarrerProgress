import React from 'react';
import axios from 'axios';

import { Card } from 'antd';

class SubjectDetail extends React.Component {

	state = {
		subject: {},
		enroll: {}
	}

	componentDidMount() {
		const subject_id = this.props.match.params.SubjectID;
		console.log(subject_id);
		axios.get(`http://127.0.0.1:8000/carrer/enroll/${subject_id}`,
			{ 'headers': {Authorization: 'token d518f3d6163fd92f074414055330022bd26fcc5a'}
			})
			.then( res => {
				this.setState({
					subject: res.data.subject,
					enroll: res.data
				});
		console.log(this.state.enroll);
			});
	}

	render () {
		return (
			<Card title={this.state.subject.name}>
				<h3><b>Estado</b>: { this.state.enroll.state }</h3>
				<h3><b>Calificacion</b>: { this.state.enroll.calification }</h3> 
				<h3><b>Anio</b>: { this.state.subject.year }</h3>
			</Card>
		)
	}
}

export default SubjectDetail;
