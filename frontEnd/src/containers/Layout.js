import React from 'react';

import { Layout, Menu, Icon } from 'antd';
import {Link} from 'react-router-dom';

const { Header, Content, Footer, Sider } = Layout;


const CustomLayout = (props) => {
	return(
		<Layout style={{ height: 1080 }}>
			<Sider
				breakpoint="lg"
				collapsedWidth="0"
				onBreakpoint={(broken) => { console.log(broken); }}
				onCollapse={(collapsed, type) => { console.log(collapsed, type); }}
			>
				<div className="logo" />
				<Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
					<Menu.Item key="1">
						<Link to="/">
							<span className="nav-text">Home</span>
						</Link>
					</Menu.Item>
					<Menu.Item key="2">
						<Link to="/1">
							<span className="nav-text">Primer Anio</span>
						</Link>
					</Menu.Item>
					<Menu.Item key="3">
						<Link to="/2">
						<span className="nav-text">Segundo Anio</span>
						</Link>
					</Menu.Item>
					<Menu.Item key="4">
						<Link to="/3">
						<span className="nav-text">Tercer Anio</span>
						</Link>
					</Menu.Item>
					<Menu.Item key="5">
						<Link to="/4">
						<span className="nav-text">Cuarto Anio</span>
						</Link>
					</Menu.Item>
					<Menu.Item key="6">
						<Link to="/5">
						<span className="nav-text">Quinto Anio</span>
						</Link>
					</Menu.Item>
				</Menu>
			</Sider>
			<Layout>
				<Header style={{ background: '#001529', padding: 0 }}> 
					<h1 style={{ color: '#fff' }}>ISI PROGRESS</h1>
				</ Header>
				<Content style={{ margin: '24px 16px 0' }}>
					<div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
						{props.children}
					</div>
				</Content>
				<Footer style={{ textAlign: 'center' }}>
					Ant Design ©2018 Created by Ant UED
				</Footer>
			</Layout>
		</Layout>
	)
}


export default CustomLayout;



