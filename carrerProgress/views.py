from django.shortcuts import(
    render,
    get_object_or_404
)

from django.http import HttpResponseRedirect
from django.http import Http404
from django.template import loader
from django.views import View
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin

# API
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .serializers import EnrolledSerializer, SubjectSerializer

# Import current User
from django.contrib.auth.models import User

# Models Import
from .models import(
    subjects,
    enrolled
)

class ListEnrolled(APIView):

    def get_objects(self, user):
        try:
            return enrolled.objects.filter(user=user)
        except enrolled.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        enroll = self.get_objects(pk)
        serializer = EnrolledSerializer(enroll, many=True)
        return Response(serializer.data)

    # Post necesario para la actualizacion del estado de la materia en curso
    def post(self, request, format=None):
        serializer = EnrolledSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DetailEnrolled(APIView):

    def get_object(self, pk):
        try:
            return enrolled.objects.get(subject=pk)
        except enrolled.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        enrolled_return = self.get_object(pk)
        serializer = EnrolledSerializer(enrolled_return)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        enrrolled_put = self.get_object(pk)
        serializer = EnrolledSerializer(enrrolled_put, data=request.data)
        if serializer.is_valid():
            serializer.save
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListSubjects(APIView):

    def get(self, request, format=None):
        subject = subjects.objects.all()
        serializer = SubjectSerializer(subject, many=True)
        return Response(serializer.data)


# Non API Views


class DetailsView(LoginRequiredMixin, View):
    login_url = '/carrer/login'
    redirect_field_name = 'redirect_to'
    template_name = 'carrerProgress/details.html'
    model = subjects
    context = {}

    def set_context(self):
        current_user = self.request.user
        id_ = self.kwargs.get("pk")
        self.context = {
            "subjects": (subjects.objects.filter(id=id_)).first(),
            "enrolled": (enrolled.objects.filter(user=current_user)).filter(subject=id_).first()
        }

    def get(self, request, *args, **kwargs):
        self.set_context()
        print( self.context)
        return render(request, self.template_name, self.context)

class IndexView(LoginRequiredMixin,View):
    login_url = '/carrer/login'
    redirect_field_name = 'redirect_to'
    template_name = 'carrerProgress/index.html'
    context = {}

    def set_context(self):
        self.context = {
            "PrimerAnio": subjects.objects.filter(year=1),
            "SegundoAnio": subjects.objects.filter(year=2),
            "TercerAnio": subjects.objects.filter(year=3),
            "CuartoAnio": subjects.objects.filter(year=4),
            "QuintoAnio": subjects.objects.filter(year=5),
            "PrimerAnioAprobado": self.get_Aprobed(1),
            "SegundoAnioAprobado": self.get_Aprobed(2),
            "TercerAnioAprobado": self.get_Aprobed(3),
            "CuartoAnioAprobado": self.get_Aprobed(4),
            "QuintoAnioAprobado": self.get_Aprobed(5),
        }

    def get_Aprobed(self, year):
        current_user = self.request.user
        filter_aprobed = enrolled.objects.filter(user=current_user)
        if filter_aprobed:
            filter_aprobed = filter_aprobed.filter(subject__year=year)
            total = len(subjects.objects.filter(year=year))
            if total > 0:
                filter_aprobed = filter_aprobed.filter(state="Aprobada")
                return {
                    "total": (len(filter_aprobed)/total)*100,
                    "materias": filter_aprobed
                }
        else: pass

    def get(self, request, *args, **kwargs):
        self.set_context()
        return render(request, self.template_name, self.context)
