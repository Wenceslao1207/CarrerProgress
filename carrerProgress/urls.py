from django.conf.urls import url
from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from rest_framework.authtoken import views as authv

app_name = 'carrerProgress'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^login/$', auth_views.LoginView.as_view(template_name='carrerProgress/login.html')),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^enroll/(?P<pk>[0-9]+)/$', views.DetailEnrolled.as_view(), name='enroll'),
    url(r'^listSubjects/$', views.ListSubjects.as_view()),
    url(r'(?P<pk>[0-9]+)/$', views.DetailsView.as_view() ,name='detail'),
    url(r'^api-token-auth/$', authv.obtain_auth_token)
]
