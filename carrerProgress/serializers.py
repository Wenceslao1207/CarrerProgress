from .models import enrolled, subjects, professor

from rest_framework import serializers

class ProfessorSerializer(serializers.ModelSerializer):
    class Meta:
        model = professor
        fields = ('id', 'name', 'last_name')

    def create(self, validated_data):
        return professor.objects.create(**validated_data)

class SubjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = subjects
        fields = ('id', 'name', 'year', 'subject_professor', 'requirements')
        depth = 0

    def create(self, valdiated_data):
        return subjects.objects.create(**valdiated_data)

    def update(self, valdiated_data):
        instance.name = valdiated_data.get('name', instance.name)
        instance.state = valdiated_data.get('year', instance.year)
        instamce.calification = validated_data.get('subject_professor', instance.subject_professor)
        instance.subject_professor = validated_data.get('requirements', instance.requirements)

class EnrolledSerializer(serializers.ModelSerializer):

    class Meta:
        model = enrolled
        fields = ('id','state','calification','year','subject')
        depth = 1

    def create(self, validated_data):
        return enrolled.objects.create(**validated_data)



