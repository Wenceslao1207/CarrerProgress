from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User


class professor (models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__ (self):
        return self.name + ' ' + self.last_name

class subjects (models.Model):
    name =models.CharField(max_length=200)
    year = models.IntegerField(default=1)
    subject_professor = models.ForeignKey(professor, on_delete=models.CASCADE)
    requirements = JSONField(null=True)

    def __str__ (self):
        return self.name

class enrolled (models.Model):
    state = models.CharField(max_length=200, default="Sin Cursar")
    calification = models.IntegerField(default=0)
    year = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    subject = models.ForeignKey(subjects, on_delete=models.CASCADE)

    def __str__ (self):
        return self.subject.name + ' ' + self.user.username

class parcial (models.Model):
    calification = models.IntegerField(default=0)
    date = models.DateTimeField('Parcial date')
    parcial_subject = models.ForeignKey(subjects, on_delete=models.CASCADE)

    def __str__ (self):
        return self.date

