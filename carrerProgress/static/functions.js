const data_set = (data) => {
		if (data != 'None') {
			var requirements; 
			requirements = data.replace(/&#39;/g,'"');
			requirements = JSON.parse(requirements);
			return ({
					"requirements":requirements,
					"show":true
			});
		} else {
			return ({ 
					"show":false,
					"requirements": 'none'
			});
		}
}

function isEmpty(obj) {
	for (var key in obj) {
		if(obj.hasOwnProperty(key))
			return false;
		}
	return true;
}

