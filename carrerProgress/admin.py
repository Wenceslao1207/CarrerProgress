from django.contrib import admin
from .models import professor, subjects, parcial, enrolled

# Register your models here.
admin.site.register(professor)
admin.site.register(subjects)
admin.site.register(parcial)
admin.site.register(enrolled)
